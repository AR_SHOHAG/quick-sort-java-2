import java.util.*;

public class QuickSort {

	public void quick_sort(int A[], int p, int r)
	{
		int q;

		if(p<r){
			q = Partition(A, p, r);
			quick_sort(A, p, q-1);
			quick_sort(A, q+1, r);
		}
	}

	public int Partition(int A[], int p, int r)
	{
		int i, j, x, temp = 0;

		x = A[r];
		i = p-1;

		for(j=p; j<r; ++j){
			if(A[j] <= x){
				++i;

				temp = A[i];
				A[i] = A[j];
				A[j] = temp;
			}
		}

		temp = A[i+1];
		A[i+1] = A[r];
		A[r] = temp;

		return (i+1);
	}

	public static void main(String[] args) {

		int size;

		Scanner in = new Scanner(System.in);

		System.out.print("Please Enter The Size of Array: ");
		size = in.nextInt();

		int A[] = new int[size];

		System.out.print("Enter " + size + " integers: ");
		for(int l =0; l<size; ++l){
			A[l] = in.nextInt();
		}

		System.out.print("Original Array: ");
		for(int m =0; m<size; ++m){
			System.out.print(A[m] + "  ");
		}

		int p, r;
		r = A.length -1;
		p = 0;

		QuickSort Q = new QuickSort();
		Q.quick_sort(A, p, r);

		System.out.print("\nSorted Array: ");
		for(int n=0; n<A.length; ++n){
            System.out.print(A[n] + "  ");
        }
        System.out.println();

	}

}
